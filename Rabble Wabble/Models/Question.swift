//
//  Question.swift
//  Rabble Wabble
//
//  Created by Роман Хоменко on 21.04.2022.
//

import Foundation

public class Question: Codable {
    public let answer: String
    public let hint: String?
    public let prompt: String
    
    public init(answer: String, hint: String?, prompt: String) {
        self.answer = answer
        self.hint = hint
        self.prompt = prompt
    }
}
