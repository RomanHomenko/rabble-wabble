//
//  SelectQuestionGroupViewController.swift
//  Rabble Wabble
//
//  Created by Роман Хоменко on 21.04.2022.
//

import UIKit

public class SelectQuestionGroupViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet internal var tableView: UITableView! {
        didSet {
            // Prevent the tableView from drawing
            // unnecessary empty table view Cells
            // which it dors by default after all other cells are drawn
            tableView.tableFooterView = UIView()
        }
    }
    
    // MARK: - Properties
    private let appSettings = AppSettings.shared
    private let questionGroupCaretaker = QuestionGroupCaretaker()
    private var questionGroups: [QuestionGroup] {
        return questionGroupCaretaker.questionGroups
    }
    private var selectedQuestionGroup: QuestionGroup! {
        get { return questionGroupCaretaker.selectedQuestionGroup }
        set { questionGroupCaretaker.selectedQuestionGroup = newValue }
    }
    
    // MARK: - View Lifecycle
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        questionGroups.forEach {
            print("\($0.title): " + "correctCount \($0.score.correctCount), " + "incorrectCount \($0.score.incorrectCount)")
        }
    }
}

// MARK: - UITableViewDataSource
extension SelectQuestionGroupViewController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionGroups.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionGroupCell") as! QuestionGroupCell
        let questionGroup = questionGroups[indexPath.row]
        cell.titleLabel.text = questionGroup.title
        
        questionGroup.score.runningPercentage.addObserver(cell, options: [.initial, .new]) { [weak cell] (percentage, _) in
            DispatchQueue.main.async {
                cell?.percentageLabel.text = String(format: "%.0f %%", round(100 * percentage))
            }
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SelectQuestionGroupViewController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectedQuestionGroup = questionGroups[indexPath.row]
        return indexPath
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Segue
extension SelectQuestionGroupViewController {
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        guard let vc = segue.destination as? QuestionViewController else { return }
//        // MARK: - The first strategy with the right order of questions
////        vc.questionStrategy = SequentialQuestionStrategy(questionGroup: selectedQuestionGroup)
//
//        // MARK: - the second strategy with the shuffled questions
//        vc.questionStrategy = appSettings.questionStrategy(for: questionGroupCaretaker)
//
//        vc.delegate = self
        if let questionVC = segue.destination as? QuestionViewController {
            questionVC.questionStrategy = appSettings.questionStrategy(for: questionGroupCaretaker)
            questionVC.delegate = self
        } else if let navController = segue.destination as? UINavigationController, let createQuestionVC = navController.topViewController as? CreateQuestionGroupViewController {
            createQuestionVC.delegate = self
        }
    }
}

// MARK: - CreateQuestionGroupViewControllerDelegate
extension SelectQuestionGroupViewController: CreateQuestionGroupViewControllerDelegate {
    public func createQuestionGroupViewControllerDidCancel(_ viewController: CreateQuestionGroupViewController) {
        
        dismiss(animated: true)
    }
    
    public func createQuestionGroupViewController(_ viewController: CreateQuestionGroupViewController, created questionGroup: QuestionGroup) {
        
        questionGroupCaretaker.questionGroups.append(questionGroup)
        try? questionGroupCaretaker.save()
        
        dismiss(animated: true)
        tableView.reloadData()
    }
}
