//
//  QuestionView.swift
//  Rabble Wabble
//
//  Created by Роман Хоменко on 21.04.2022.
//

import UIKit

public class QuestionView: UIView {
    @IBOutlet public var answerLabel: UILabel!
    @IBOutlet public var correctCountLabel: UILabel!
    @IBOutlet public var incorrectCountLabel: UILabel!
    @IBOutlet public var promptLabel: UILabel!
    @IBOutlet public var hintLabel: UILabel!
}
