//
//  QuestionGroupCell.swift
//  Rabble Wabble
//
//  Created by Роман Хоменко on 21.04.2022.
//

import UIKit

public class QuestionGroupCell: UITableViewCell {
    @IBOutlet public var titleLabel: UILabel!
    @IBOutlet public var percentageLabel: UILabel!
}
